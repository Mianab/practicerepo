<?php

namespace App\Http\Controllers;

use App\Appointment;
use Illuminate\Http\Request;
use App\User;
use Auth;
class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { $appointments = "";

    
        if(Auth::user()->role == "doctor")
        {
        $appointments = Appointment::where('doc_id',Auth::user()->id)->get();
       }
       else{
        $appointments = Appointment::all();
       }
        return view("view-all",compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set("Asia/Karachi");
        $appointment = new Appointment();
        $appointment->name =  $_REQUEST['name'];
        $appointment->disease =$_REQUEST['disease'];
        $appointment->gender =$_REQUEST['gender'];
        $appointment->phone =$_REQUEST['phone'];
        $appointment->doc_id =$_REQUEST['doctor_id'];
        $appointment->save();
        return redirect('/view-all');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $appointment = Appointment::find($id);
         return view('edit', compact('appointment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
                 $appointment = Appointment::find($id);
                   $appointment->name =  $_REQUEST['name'];
        $appointment->disease =$_REQUEST['disease'];
        $appointment->gender =$_REQUEST['gender'];
        $appointment->phone =$_REQUEST['phone'];
        $appointment->created_at =$_REQUEST['appointment_date'];
        $appointment->save();
        return redirect('/view-all');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = Appointment::find($id);
        $appointment->delete();
        return redirect('/view-all');
    }
}
