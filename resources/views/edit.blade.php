<!DOCTYPE html>
<html>
@include('includes.head')
<body>
@include('includes.topnav')
@include('includes.nav')

<div class="col-md-4">
  <div class="row">
    <form class="form" action="/update-appointment/{{$appointment->id}}" method="get">

        <input type="text" class="form-control" name="name" value="{{ $appointment->name }}">

        <input type="phone" name="phone" class="form-control" id="phone" placeholder="Enter Your Phone#" value="{{ $appointment->phone }}" required >

        <input type="disease" name="disease" class="form-control" id="disease" placeholder="Your Disease" value="{{ $appointment->disease }}" required>
        <br>
        <label for="gender">Select Gender:</label>
                <select  class="form-control" name="gender" required>
                <option >---</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Others">Others</option>

                </select>
            <br>
      <br>
      <input type="date" name="appointment_date">

          <button type="submit" class="btn btn-success">Submit</button>

    </form>
  </div>
</div>




</body>
</html>

