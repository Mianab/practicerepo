<!DOCTYPE html>
<html>
@include('includes.head')
<body>
@include('includes.topnav')
@include('includes.nav')

<div class="col-md-12">
	<div class="row col-md-4" style="margin-left: 30%">
		<center>
		<form class="form" action="/save-appointment" method="get">

				<input type="name" name="name" class="form-control" id="name" placeholder="Enter Your Name"value="{{Auth::user()->name}}" required readonly>
				<input type="phone" name="phone" class="form-control" id="phone" placeholder="Enter Your Phone#" required>
				<input type="disease" name="disease" class="form-control" id="disease" placeholder="Your Disease" required>
				<br>
				<label for="gender">Select Gender:</label>
                <select  class="form-control" name="gender" required>
                <option >---</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Others">Others</option>

                </select>
            <br>
			<br>
			<label>Select Doctor</label>
			<select name="doctor_id" class="form-control" required>
			    <option >---</option>
 
				@foreach($doctorsList as $doctor)
				<option value="{{$doctor->id}}">{{$doctor->name}}</option>
				@endforeach
			</select>
			<br>
			    <button type="submit" class="btn btn-success">Submit</button>

		</form>
	</center>
	</div>
</div>



@include('includes.footer')

</body>
</html>

