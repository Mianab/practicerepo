<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h4> HMS </h4>
                </div>
                <div class="col-lg-3 col-sm-2 col-xs-3">
                    <h3> Contact </h3>
                    <ul>
                        <li><a class="email"href="#">  info@hms.org.pk </a></li>
                        <br/>
                        <li> <p> 03174234495 </p> </li>
                        <li> <p> 03075493274 </p> </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-2 col-xs-3">
                    <ul>
                        <li> <h5> <a href="#" style="margin-top: 5em"> ABOUT US</a> <h5></li>
                        <li> <h5><a href="#"> CURRENT Trustee </a> <h5></li>
                        <li> <h5><a href="#"> Our Vision </a> <h5></li>
                        <li> <h5><a href="#"> Location </a> <h5></li>
                    </ul>
                </div>
               
            <!--/.row--> 
        </div>
        <!--/.container--> 
    </div>
    <!--/.footer-->
                          
    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left copyright"> Copyright © HMS 2020. All right reserved. </p>
       
        </div>
    </div>
    <!--/.footer-bottom--> 

</footer>