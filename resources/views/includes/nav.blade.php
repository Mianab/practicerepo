<nav style="background-color: #77cc6d" class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <a style="color: white;" class="navbar-brand logoName" href="#">HMS</a>
    </div>
    <ul style="float: right; " class="nav navbar-nav">
      <li class="active"><a href="#">HOME</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT<span class="caret"></span></a>
        <ul class="dropdown-menu dropDownCustom">
          <li><a href="#">IMPORTANT MESSAGES</a></li>
          <li><a href="#">ACHIEVEMENTS</a></li>
          <li><a href="#">BOARD FACULTY</a></li>
          <li><a href="#">ACOUNTS</a></li>
        </ul>
      </li>

            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">DEPARTMENTS<span class="caret"></span></a>
        <ul class="dropdown-menu dropDownCustom">
          <li><a href="#">DENTAL SURGERY</a></li>
          <li><a href="#">PHYSICIAN</a></li>
          <li><a href="#">ORTHOPAEDIC</a></li> 
          <li><a href="#">PSYCHIATRY</a></li>
        </ul>
      </li>

      <li><a href="#">SERVICES</a></li>
      <li><a href="#">GALLERY</a></li>
      <li><a href="#">CONTACTS</a></li>
    </ul>
  </div>
</nav>
