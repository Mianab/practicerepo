<div class="header">
  <a href="#" class="logo"><i class="fa fa-phone" aria-hidden="true"></i> 03075493274  </a>
  <a href="#" class="logo"> | <i class="fa fa-envelope" aria-hidden="true"></i> info@hms.org.pk</a>
  <a href="#" class="logo"> | <i class="fa fa-facebook" aria-hidden="true"></i> </a>
  <a href="#" class="logo"> | <i class="fa fa-twitter" aria-hidden="true"></i> </a>
  <div class="header-right">
    <a class="active" href="#home">Make a Donation</a>
    <a href="#contact">Hospital News</a>
    <a href="#about">Audit Reports</a>
    <a href="#about">Our Affiliations</a>
    <a href="#about">Employee Email</a>
                                @auth
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                 @else
                                 <a href="/login">
                                            Login
                                        </a>
                                 @endif     
                                
  </div>
</div>