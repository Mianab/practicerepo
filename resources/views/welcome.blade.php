<!DOCTYPE html>
<html>
@include('includes.head')
<body>

  <!-- starting header -->
@include('includes.topnav')
<!-- first Section-->

@include('includes.nav')
<!-- second section-->

  <!--Carousal-->

<div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="images/beds.png" alt="images" style="width:100%; height: 500px;">
        <div style="margin-bottom: 150px;" class="carousel-caption">
          <h1>Quality Health For All At No Cost </h1>
          <h3>Your Money Can Save A Valuable Life</h3>
        </div>
      </div>

      <div class="item">
        <img src="images/doo.png" alt="images" style="width:100%; height: 500px;">
        <div style="margin-bottom: 150px;" class="carousel-caption">
          <h1>Serving Your Health with Excellence</h1>
          <h3>A family of hospitals for your family</h3>
        </div>
      </div>
    
      <div class="item">
        <img src="images/staff.png" alt="images" style="width:100%; height: 500px;">
        <div style="margin-bottom: 150px;" class="carousel-caption">
          <h1>A family of hospitals for your family</h1>
          <h3>In Love with Life</h3>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<!-- third section-->
<!-- <div class="col-md-4">
<div class="row1">
    
      <a style="text-decoration: none;" href="#"><i class="fa fa-user-md" aria-hidden="true"></i><br></a>
    
    
  </div>
  
</div>
-->
<div class="col-md-12">
<div class="row1">
    
      <a style="text-decoration: none; text-align: center;" href="/book-an-appointment"><i class="fa fa-ticket" aria-hidden="true"></i><br>Make Appointment</a>
    
    
  </div>
  
</div>
<!--
<div class="col-md-4">
<div class="row1">
    
      <a style="text-decoration: none;" href="#"><i class="fa fa-user-md" aria-hidden="true"></i><br>Appoint Your Doctor</a>
    
    
  </div>
  
</div>
-->


<!-- Footer-->

@include('includes.footer')

</body>
</html>