<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
if (Auth::check()) {
 if(Auth::user()->role == "doctor")
        {
        return redirect('/view-all');


        }
    }

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/book-an-appointment', function () {

$doctorsList = DB::table('users')->where('role','doctor')->get();

    return view('form',compact('doctorsList'));
})->middleware('auth');

Route::get('/save-appointment', 'AppointmentController@store');

Route::get('/view-all', 'AppointmentController@index');

Route::get('/appointment/{id}/edit', 'AppointmentController@edit');


Route::get('/appointment/{id}/delete', 'AppointmentController@destroy');
Route::get('/update-appointment/{id}', 'AppointmentController@update');


